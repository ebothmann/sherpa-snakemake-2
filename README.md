# Installation

Requirements:
- `snakemake` (can be installed via pip)
- possibly `conda`, to switch to Python 2 for Rivet 2, which can enabled by using `snakemake`’s `--use-conda` switch

Just make sure that the sherpa-snakemake directory is part of your path:

```sh
export PATH="path/to/sherpa-snakemake:$PATH"
```

# Usage

Create a directory with a `snakemake.yaml` and a `Sherpa.yaml` as demonstrated
with `test-setup/`. Then simply run

```sh
sherpa-snakemake -j all
```
