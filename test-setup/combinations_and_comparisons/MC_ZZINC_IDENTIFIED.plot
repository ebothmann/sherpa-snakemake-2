# BEGIN PLOT /MC_ZZINC_IDENTIFIED/xs$
Title=Total cross section
YLabel=$\sigma$ [pb]
RatioPlotYMin=0.95
RatioPlotYMax=1.05
# END PLOT

# BEGIN PLOT /MC_ZZINC_IDENTIFIED/ZZ_pT$
Title=Transverse momentum of boson pair
XLabel=$p_\perp^{\text{ZZ}}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_\perp^{\text{ZZ}}$ [pb/GeV]
LogX=1
LegendXPos=0.05
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_ZZINC_IDENTIFIED/ZZ_pT_peak
Title=Peak region of transverse momentum of boson pair
XLabel=$p_\perp^{\text{ZZ}}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_\perp^{\text{ZZ}}$ [pb/GeV]
LogY=0
LegendYPos=0.5
LegendXPos=0.30
# END PLOT

# BEGIN PLOT /MC_ZZINC_IDENTIFIED/ZZ_eta
Title=Pseudorapidity of boson pair
XLabel=$\eta_{\text{ZZ}}$
YLabel=$\text{d}\sigma/\text{d}\eta_{\text{ZZ}}$ [pb]
LegendXPos=0.35
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_ZZINC_IDENTIFIED/ZZ_phi
Title=Azimuthal angle of boson pair
XLabel=$\phi_{\text{ZZ}}$
YLabel=$\text{d}\sigma/\text{d}\phi_{\text{ZZ}}$ [pb]
LogY=0
# END PLOT

# BEGIN PLOT /MC_ZZINC_IDENTIFIED/ZZ_m
Title=Invariant mass of boson pair
XLabel=$m_{\text{ZZ}}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m_{\text{ZZ}}$ [pb/GeV]
LogX=1
LegendXPos=0.05
LegendYPos=0.5
XMin=160
XMax=2000
# END PLOT

# BEGIN PLOT /MC_ZZINC_IDENTIFIED/ZZ_dphi
Title=Azimuthal angle difference of boson pair
XLabel=$\Delta\phi_{\text{ZZ}}$
YLabel=$\text{d}\sigma/\text{d}\Delta\phi_{\text{ZZ}}$ [pb]
LegendXPos=0.10
# END PLOT

# BEGIN PLOT /MC_ZZINC_IDENTIFIED/ZZ_deta
Title=Pseudorapidity difference of boson pair
XLabel=$\Delta\eta_{\text{ZZ}}$
YLabel=$\text{d}\sigma/\text{d}\Delta\eta_{\text{ZZ}}$ [pb]
LegendXPos=0.35
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_ZZINC_IDENTIFIED/ZZ_dR
Title=Separation of boson pair
XLabel=$\Delta R_{\text{ZZ}}$
YLabel=$\text{d}\sigma/\text{d}\Delta R_{\text{ZZ}}$ [pb]
LegendXPos=0.35
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_ZZINC_IDENTIFIED/ZZ_dpT
Title=Transverse momentum difference of boson pair
XLabel=$\Delta p_\perp^{\text{ZZ}}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}\Delta p_\perp^{\text{ZZ}}$ [pb/GeV]
LogX=1
LegendXPos=0.05
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_ZZINC_IDENTIFIED/ZZ_costheta_planes
Title=Angle between the boson decay planes
XLabel=$\cos(\Psi_{\text{e}\nu,\mu\nu})$
YLabel=$\text{d}\sigma/\text{d}\cos(\Psi_{\text{e}\nu,\mu\nu})$ [pb]
# END PLOT

# BEGIN PLOT /MC_ZZINC_IDENTIFIED/Z_pT
Title=Z $p_\perp$
XLabel=$p_\perp^{\text{Z}}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_\perp^{\text{Z}}$ [pb/GeV]
LogX=1
LegendXPos=0.05
LegendYPos=0.5
XMin=10
XMax=2000
# END PLOT

# BEGIN PLOT /MC_ZZINC_IDENTIFIED/Z_eta
Title=Z pseudorapidity
XLabel=$\eta_{\text{Z}}$
YLabel=$\text{d}\sigma/\text{d}\eta_{\text{Z}}$ [pb]
LegendXPos=0.35
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_ZZINC_IDENTIFIED/Zl_pT
Title=Lepton $p_\perp$
XLabel=$p_\perp^{\text{l}}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_\perp^{\text{l}}$ [pb/GeV]
LogX=1
# END PLOT

# BEGIN PLOT /MC_ZZINC_IDENTIFIED/Zl_eta
Title=Lepton $\eta$
XLabel=$\eta_{\text{l}}$
YLabel=$\text{d}\sigma/\text{d}\eta_{\text{l}}$ [pb]
# END PLOT

# BEGIN PLOT /MC_ZZINC_IDENTIFIED/ZeZm_dphi
Title=Azimuthal angle difference between oppositely charged leptons
XLabel=$\Delta\phi_{e^+,\mu^-}$
YLabel=$\text{d}\sigma/\text{d}\Delta\phi$ [pb]
LegendXPos=0.10
# END PLOT

# BEGIN PLOT /MC_ZZINC_IDENTIFIED/ZeZm_deta
Title=Pseudorapidity difference between oppositely charged leptons
XLabel=$\Delta\eta_{e^+,\mu^-}$
YLabel=$\text{d}\sigma/\text{d}\Delta\eta$ [pb]
LegendXPos=0.30
LegendYPos=0.5
# END PLOT

# BEGIN PLOT /MC_ZZINC_IDENTIFIED/ZeZm_dR
Title=Separation between oppositely charged leptons
XLabel=$\Delta R_{e^+,\mu^-}$
YLabel=$\text{d}\sigma/\text{d}\Delta R$ [pb]
# END PLOT

# BEGIN PLOT /MC_ZZINC_IDENTIFIED/ZeZm_m
Title=Invariant mass of oppositely charged leptons
XLabel=$m_{e^+,\mu^-}$ [GeV]
YLabel=$\text{d}\sigma/\text{d}m$ [pb/GeV]
# END PLOT
