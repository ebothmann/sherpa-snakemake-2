// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"

namespace Rivet {

/// @brief MC validation analysis for (unphysical) ZZ events
class MC_ZZINC_IDENTIFIED : public Analysis {

public:
  /// Default constructor
  MC_ZZINC_IDENTIFIED() : Analysis("MC_ZZINC_IDENTIFIED") {}

  /// @name Analysis methods
  //@{

  /// Book histograms
  void init() {

    getLog().setLevel(Log::WARN);

    FinalState fs;
    IdentifiedFinalState zfinder(fs);
    zfinder.acceptIdPair(23);
    declare(zfinder, "ZFinder");

    // properties of the pair momentum
    double sqrts = 100000.;
     _h_ZZ_pT = bookHisto1D("ZZ_pT", logspace(100, 1.0, 0.5 * sqrts));
     _h_ZZ_pT_peak = bookHisto1D("ZZ_pT_peak", 25, 0.0, 25.0);
     _h_ZZ_eta = bookHisto1D("ZZ_eta", 40, -7.0, 7.0);
     _h_ZZ_phi = bookHisto1D("ZZ_phi", 25, 0.0, TWOPI);
     _h_ZZ_m = bookHisto1D("ZZ_m", logspace(100, 150.0, 180.0 + 0.25 * sqrts));

    // correlations between the ZZ
     _h_ZZ_dphi = bookHisto1D("ZZ_dphi", 25, 0.0, PI); /// @todo non-linear?
     _h_ZZ_deta = bookHisto1D("ZZ_deta", 25, -7.0, 7.0);
     _h_ZZ_dR = bookHisto1D("ZZ_dR", 25, 0.5, 7.0);
     _h_ZZ_dpT = bookHisto1D("ZZ_dpT", logspace(100, 1.0, 0.5 * sqrts));

    /// @todo fuer ZZ: missing ET

    // properties of the Z bosons
     _h_Z_pT = bookHisto1D("Z_pT", logspace(100, 10.0, 0.25 * sqrts));
     _h_Z_eta = bookHisto1D("Z_eta", 70, -7.0, 7.0);

     _h_xs = bookHisto1D("xs", 1, -0.5, 0.5);
  }

  /// Do the analysis
  void analyze(const Event &e) {

    const auto& w = e.weight();

    const IdentifiedFinalState &zfinder =
        apply<IdentifiedFinalState>(e, "ZFinder");
    Particles zbosons = zfinder.particles();
    MSG_DEBUG(zbosons.size());
    if (zbosons.size() != 2) {
      vetoEvent;
    }

    _h_xs->fill(0.0, w);

    FourMomentum zenu(zbosons[0].momentum());
    FourMomentum zmnu(zbosons[1].momentum());
    FourMomentum zz(zenu + zmnu);

    _h_ZZ_pT->fill(zz.pT(), w);
    _h_ZZ_pT_peak->fill(zz.pT(), w);
    _h_ZZ_eta->fill(zz.eta(), w);
    _h_ZZ_phi->fill(zz.phi(), w);
    double mzz2 = zz.mass2();
    if (mzz2 > 0.0)
      _h_ZZ_m->fill(sqrt(mzz2), w);

    _h_ZZ_dphi->fill(mapAngle0ToPi(zenu.phi() - zmnu.phi()), w);
    _h_ZZ_deta->fill(zenu.eta() - zmnu.eta(), w);
    _h_ZZ_dR->fill(deltaR(zenu, zmnu), w);
    _h_ZZ_dpT->fill(fabs(zenu.pT() - zmnu.pT()), w);

    _h_Z_pT->fill(zenu.pT(), w);
    _h_Z_pT->fill(zmnu.pT(), w);
    _h_Z_eta->fill(zenu.eta(), w);
    _h_Z_eta->fill(zmnu.eta(), w);
  }

  /// Finalize
  void finalize() {
    const double norm = crossSection() / picobarn / sumOfWeights();
    scale(_h_xs, norm);
    scale(_h_ZZ_pT, norm);
    scale(_h_ZZ_pT_peak, norm);
    scale(_h_ZZ_eta, norm);
    scale(_h_ZZ_phi, norm);
    scale(_h_ZZ_m, norm);
    scale(_h_ZZ_dphi, norm);
    scale(_h_ZZ_deta, norm);
    scale(_h_ZZ_dR, norm);
    scale(_h_ZZ_dpT, norm);
    scale(_h_Z_pT, norm);
    scale(_h_Z_eta, norm);
  }

  //@}

private:
  /// @name Histograms
  //@{
  Histo1DPtr _h_xs;
  Histo1DPtr _h_ZZ_pT;
  Histo1DPtr _h_ZZ_pT_peak;
  Histo1DPtr _h_ZZ_eta;
  Histo1DPtr _h_ZZ_phi;
  Histo1DPtr _h_ZZ_m;
  Histo1DPtr _h_ZZ_dphi;
  Histo1DPtr _h_ZZ_deta;
  Histo1DPtr _h_ZZ_dR;
  Histo1DPtr _h_ZZ_dpT;
  Histo1DPtr _h_Z_pT;
  Histo1DPtr _h_Z_eta;
  //@}
};

// The hook for the plugin system
DECLARE_RIVET_PLUGIN(MC_ZZINC_IDENTIFIED);

} // namespace Rivet
