// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/AnalysisHandler.hh"

namespace Rivet {


  /// @brief Add a short analysis description here
  class MY_XS : public Analysis {
  public:

    /// Constructor
    DEFAULT_RIVET_ANALYSIS_CTOR(MY_XS);


    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {
      book(_xs, "xs", 1, 0.0, 1.0);
    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      /// @todo Do the event by event analysis here
      _xs->fill(0.5);
    }


    /// Normalise histograms etc., after the run
    void finalize() {

      const double s = crossSection()/picobarn/sumOfWeights();
      scale(_xs, s);

    }

    //@}

    /// @name Histograms
    //@{ 
    Histo1DPtr _xs;
    //@}


  };


  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(MY_XS);


}
