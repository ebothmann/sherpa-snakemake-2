from __future__ import print_function, division

import yoda
import math
import os

def makeoutdir(out_file):
    try:
        os.makedirs(os.path.dirname(out_file))
    except OSError:
        pass

def find_key(key, dos):
    # remove option
    comps = []
    for c in key.split("/"):
        comps.append(c.split(":")[0])
    key = "/".join(comps)
    for k, v in dos.items():
        if key in k:
            return k
    return None

def combine(in_files, out_file, operand, is_correlated):

    data_object_lists = []
    for path in in_files:
        data_object_lists.append(yoda.readYODA(path))

    new_data_objects = []

    for key, obj in data_object_lists[0].items():
        new_data_object = obj.mkScatter()
        coord = new_data_object.dim()

        # check if data_object for key exists in all in_files, otherwise ignore
        # this data_object
        should_ignore = False
        for data_objects in data_object_lists[1:]:
            if find_key(key, data_objects) is None:
                should_ignore = True
                break
        if should_ignore:
            continue

        for i, p in enumerate(new_data_object.points()):
            old_val = p.val(coord)
            val = old_val

            if is_correlated:
                errs = p.errs(coord)
            else:
                if operand in ["+", "-"]:
                    errs = [e**2 for e in p.errs(coord)]
                else:
                    if not val == 0.0:
                        errs = [(e/val)**2 for e in p.errs(coord)]

            for data_objects in data_object_lists[1:]:
                k = find_key(key, data_objects)
                var_p = data_objects[k].mkScatter().points()[i]
                var_val = var_p.val(coord)

                if is_correlated:
                    if operand == "+":
                        var_errs = var_p.errs(coord)
                        errs = [x + y for x, y in zip(errs, var_errs)]
                else:
                    if operand in ["+", "-"]:
                        var_errs = [e**2 for e in var_p.errs(coord)]
                    else:
                        if not val == 0.0:
                            var_errs = [(e/val)**2 for e in var_p.errs(coord)]
                    errs = [x + y for x, y in zip(errs, var_errs)]

                if operand == "+":
                    val += var_val
                elif operand == "-":
                    val -= var_val
                elif operand == "/":
                    if var_val == 0.0:
                        val = 0.0
                    else:
                        val /= var_val
                elif operand == "*":
                    val *= var_val

            p.setVal(coord, val)

            if is_correlated:
                if operand == "+":
                    p.setErrs(coord, errs)
                elif old_val == 0.0:
                    p.setErrs(coord, [0.0, 0.0])
                else:
                    p.setErrs(coord, [e * val / old_val for e in errs])
            else:
                if operand in ["+", "-"]:
                    p.setErrs(coord, [math.sqrt(e) for e in errs])
                else:
                    p.setErrs(coord, [val*math.sqrt(e) for e in errs])

        new_data_objects.append(new_data_object)

    makeoutdir(out_file)
    yoda.writeYODA(new_data_objects, out_file)

# vim: ft=python sw=4 ts=4 et
