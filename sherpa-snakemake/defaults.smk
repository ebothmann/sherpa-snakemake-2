# the following two settings can be useful if you want to avoid installing
# LaTeX on your cluster
config.setdefault("plotting_singularity_image", "shub://ebothmann/sherpa-singularity:plotting_centos6")
config.setdefault("use_singularity_for_plotting", False)

# the following settings control how many runs are done and premerged (note
# that they can be overriden per run)
config.setdefault("run_number_offset", 0)
config.setdefault("number_of_runs", 1)
config.setdefault("premerge_multiplicity", 1)

config.setdefault("ignore_missing", False)

# MPI settings
config.setdefault("max_number_of_mpi_nodes", 1)

# Cluster settings
config.setdefault("mem_mb", {"integration": 2048, "initialization": 2048, "run": 2048})
config.setdefault("walltime_hours", {"integration": 0, "initialization": 0, "run": 0})
config.setdefault("walltime_minutes", {"integration": 59, "initialization": 59, "run": 59})

config.setdefault("verbose", False)
config.setdefault("comparisons", {"default": {"input": ["any.any"]}})
config.setdefault("global_combinations", {})
config.setdefault("external_analysis_output", {})
config.setdefault("colors", {})

config.setdefault("conda_envfile", "")

sherpa_keys = []
default_integrations = {}
try:
    sherpa_keys = list(config["sherpas"].keys())
    for key in sherpa_keys:
        default_integrations[key] = {"sherpa": key}
except KeyError:
    pass
config.setdefault("integrations", default_integrations)

def get_rivet_settings():
    settings = config["rivet"]
    settings.setdefault("analyses", [])
    settings.setdefault("custom_files", [])
    settings.setdefault("custom_source_files", [])
    settings.setdefault("args", [])
    settings.setdefault("base", "3.0")
    yoda_extension = "yoda.gz"
    should_use_multiweights = True
    if settings["base"] < "3.0":
        yoda_extension = "yoda"
        should_use_multiweights = False
    settings.setdefault("yoda_ext", yoda_extension)
    settings.setdefault("should_use_multiweights", should_use_multiweights)
    settings.setdefault("sherpa_interface", {})
    return settings

def get_comparison_settings(name):
    try:
        settings = config["comparisons"][name]
    except KeyError:
        settings = {}
    settings.setdefault("input", ["any.any"])
    settings.setdefault("plotters", ["rivet"])
    settings.setdefault("rivetmkhtml_plot_args", {})
    return settings

def get_run_settings(run_name):
    settings = config["runs"][run_name]
    settings.setdefault("sherpa", list(config["sherpas"].keys())[0])
    settings.setdefault("integration", settings["sherpa"])
    settings.setdefault("variation", "None")
    settings.setdefault("combinations", {})
    settings.setdefault("args", [])
    settings.setdefault("run_number_offset", config["run_number_offset"])
    settings.setdefault("number_of_runs", config["number_of_runs"])
    settings.setdefault("premerge_multiplicity", config["premerge_multiplicity"])
    return settings

def get_integration_settings(integration_name):
    settings = config["integrations"][integration_name]
    settings.setdefault("sherpa", list(config["sherpas"].keys())[0])
    settings.setdefault("args", [])
    return settings

def get_sherpa_settings(sherpa_name):
    settings = config["sherpas"][sherpa_name]
    settings.setdefault("base", "3.0")
    settings.setdefault("use_positional_args", True)
    settings.setdefault("plugins", {})
    return settings

def get_variations(run_name):
    run_settings = get_run_settings(run_name)
    if run_settings["variation"] in ["None", ""]:
        return {}
    else:
        raw_variations = config["variations"][run_settings["variation"]]
        variations = {}
        for key, value in raw_variations.items():
            if isinstance(value, str):
                variations[key] = {'sherpa_tag': value}
            else:
                variations[key] = value
        return variations

def get_external_analysis_output_settings(name):
    try:
        settings = config["external_analysis_output"][name]
    except KeyError:
        settings = {}
    return settings

# vim: ft=python
