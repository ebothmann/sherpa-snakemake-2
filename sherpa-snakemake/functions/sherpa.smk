def sherpa_binary(wildcards):
    return sherpa_prefix(wildcards) + "/bin/Sherpa"

def sherpa_prefix(wildcards):
    return sherpa_settings(wildcards)["prefix"]

def sherpa_analyses_args(wildcards):
    if wildcards.ext == "yaml":
        return "[" + ", ".join(get_rivet_settings()["analyses"]) + "]"
    else:
        return " ".join(get_rivet_settings()["analyses"])

def sherpa_analyses_settings(wildcards):
    lines = []
    for k, v in get_rivet_settings()["sherpa_interface"].items():
        if wildcards.ext == "yaml":
            fmt = "{}: {}"
        else:
            fmt = "{} {}"
        lines.append(fmt.format(k, v))
    if len(lines):
        return "  " + "\n  ".join(lines)
    else:
        return ""

def sherpa_config_args(wildcards):
    configs = sherpa_configs(wildcards)
    did_detect_dat_files = False
    did_detect_yml_files = False
    for c in configs:
        if c[-4:] == ".dat":
            did_detect_dat_files = True
        elif c[-4:] in ("yaml", ".yml"):
            did_detect_yml_files = True
    if did_detect_yml_files and did_detect_dat_files:
        raise Exception("Did detect both dat and yaml Sherpa config files.\n"
                "However, a given Sherpa version can only handle one type. Abort.")
    if did_detect_dat_files:
        num_user_files = len([c for c in configs if not c[:10] == "Snakemake_"])
        if num_user_files > 1:
            raise Exception("Did detect several dat Sherpa config files.\n"
                    "However, no Sherpa version can handle more than one dat Sherpa config file. Abort.")
    if did_detect_dat_files:
        args = ["-f", configs[0]]
        for c in configs[1:]:
            if c == "Snakemake_Analysis.dat":
                args.append("ANALYSIS_DATA_FILE={}".format(c))
            else:
                raise Exception("Did encounter unknown snakemake-provided"
                        " dat  Sherpa config file. Abort.")
    else:
        if sherpa_settings(wildcards)["use_positional_args"]:
            args = configs
        else:
            args = ['RUNDATA: [' + ', '.join(configs) + ']']
    return args

def sherpa_configs(wildcards):
    run_settings = run_or_integration_settings(wildcards)
    run_sherpa_settings = sherpa_settings(wildcards)
    configs = []
    try:
        configs.extend(run_settings["sherpa_configs"])
    except KeyError:
        if run_sherpa_settings["base"] >= "3.0":
            configs = ["Sherpa.yaml"]
        else:
            configs = ["Run.dat"]
    if "run" in wildcards.keys():
        if run_sherpa_settings["base"] >= "3.0":
            configs.append("Snakemake_Analysis.yaml")
        else:
            configs.append("Snakemake_Analysis.dat")
    return configs

def sherpa_plugins(wildcards):
    """Provide plugin files for running Sherpa."""
    run_sherpa_settings = sherpa_settings(wildcards)
    sherpa_dir = "sherpa/" + wildcards.integ + "/"
    return [sherpa_dir + f for f in run_sherpa_settings["plugins"].keys()]

def sherpa_plugin_input(wildcards):
    return sherpa_plugin_source_files(wildcards)

def sherpa_plugin_source_files(wildcards):
    run_sherpa_settings = sherpa_settings(wildcards)
    sherpa_dir = "sherpa/" + wildcards.integ + "/"
    plugin_settings = run_sherpa_settings["plugins"][wildcards.plugin + ".so"]
    return ["sherpa_plugins/" + f for f in plugin_settings["source_files"]]

def sherpa_settings(wildcards):
    settings = run_or_integration_settings(wildcards)
    return get_sherpa_settings(settings["sherpa"])

def run_or_integration_settings(wildcards):
    if "run" in wildcards.keys():
        settings = get_run_settings(wildcards.run)
    else:
        settings = get_integration_settings(wildcards.integ)
    return settings

# vim: ft=python
