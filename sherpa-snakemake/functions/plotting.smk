def comparison_plot_directories(wildcards):
    keys = []
    dirs = []
    for key in config["comparisons"].keys():
        settings = get_comparison_settings(key)
        for p in settings["plotters"]:
            keys.append(key)
            dirs.append(p + "-plots")
    return expand("comparisons/{key}/{directory}", zip, key=keys, directory=dirs)


def custom_rivet_plugin(wildcards):
    source_files = get_rivet_settings()["custom_source_files"]
    if len(source_files) > 0:
        return "RivetAnalysis.so"
    else:
        return []


def comparison_input_histogram_files(wildcards):
    """Returns the list of histogram output files needed to make a comparison.
    This is not equal to the files that are actually passed to the comparison
    command, because we never track Sherpa variations as a separate dependency,
    hence only the associated nominal histogram output will be added to the
    returned list."""
    comparison = get_comparison_settings(wildcards.comparison)
    names = comparison["input"]
    files = []
    for name in names:
        if "." in name:
            run, var = name.split(".")
            if run == "any":
                runs = config["runs"].keys()
            else:
                runs = [run]
            for run in runs:
                run_settings = get_run_settings(run)
                variations = get_variations(run)
                # if *any variation* is needed for the comparison, we add the
                # nominal histogram output to the list of files
                if var == "any" or var == "nominal" or var in variations.keys():
                    files.append(
                            histogram_output_file("analysis_output", run, run_settings))
                # var could instead be a combination label; for *each
                # combination* that is needed for the comparison, we add its
                # histogram output to the list of files
                for combination in run_settings["combinations"]:
                    if var == "any" or var == combination:
                        var_sherpa_tag = "." + combination
                        files.append(histogram_output_file(
                            "combined_analysis_output", run, run_settings,
                            tag=var_sherpa_tag))
        else:
            if name in config["global_combinations"].keys() or name == "any":
                settings = get_global_combination_settings(name)
                files.append(histogram_output_file(
                    "combined_analysis_output", name, settings))
            if name in config["external_analysis_output"].keys() or name == "any":
                settings = get_external_analysis_output_settings(name)
                files.append("external_analysis_output/" + settings["file"])
    return files


def get_plotinfo(comparison_settings, index):
    plotinfo = {}
    for key in ["colors", "styles", "error_bars", "error_bands", "error_tubes", "legend_order", "error_band_styles", "error_band_colors", "hatch_colors", "line_widths", "line_dash", "scales"]:
        plotinfo[key] = None
        if key in comparison_settings.keys():
            settings = comparison_settings[key]
            if len(settings) > index:
                plotinfo[key] = settings[index]
    return plotinfo

def dressing_for_plotinfo(pi):
    if pi is None:
        return ""
    dressing = ""
    for key, value in pi.items():
        if value is None or value == "None":
            continue
        if key == "colors":
            dressing += ":LineColor=" + get_color(value)
        elif key == "styles":
            dressing += ":LineStyle=" + value
        elif key == "error_bars":
            dressing += ":ErrorBars={}".format(value)
        elif key == "error_bands":
            dressing += ":ErrorBands={}".format(value)
        elif key == "error_tubes":
            dressing += ":ErrorTubes={}".format(value)
        elif key == "error_band_styles":
            dressing += ":ErrorBandStyle={}".format(value)
        elif key == "error_band_colors":
            dressing += ":ErrorBandColor={}".format(get_color(value))
        elif key == "hatch_colors":
            dressing += ":HatchColor={}".format(get_color(value))
        elif key == "legend_order":
            dressing += ":LegendOrder={}".format(value)
        elif key == "line_widths":
            dressing += ":LineWidth={}".format(value)
        elif key == "line_dash":
            dressing += ":LineDash={}".format(value)
        elif key == "scales":
            dressing += ":Scale={}".format(value)
    return dressing

def get_color(color_name):
    colors = config["colors"]
    if color_name in colors:
        if isinstance(colors[color_name], str):
            return colors[color_name]
        else:
            color_string = "{[rgb]{"
            color_string += ",".join([str(comp/255) for comp in colors[color_name]])
            color_string += "}}"
            return color_string
    else:
        # assuming that make-plots can handle this
        return color_name

def comparison_input_args(wildcards, dressed=True):
    comparison = get_comparison_settings(wildcards.comparison)
    names = comparison["input"]
    files = []
    for name in names:
        if "." in name:
            run, var = name.split(".")
            if run == "any":
                runs = config["runs"].keys()
            else:
                runs = [run]
            for run in runs:
                run_settings = get_run_settings(run)
                variations = dict(get_variations(run))
                has_variations = len(variations) > 0
                variations[""] = {"sherpa_tag": "Weight"}
                for current_var in sorted(variations.keys()):
                    if (not var == "any"
                            and not current_var == var
                            and not (var == "nominal" and current_var == "")):
                        continue
                    name = "nominal"
                    if current_var == "":
                        var_sherpa_tag = ""
                    else:
                        name = current_var
                        var_sherpa_tag = "." + current_var
                    try:
                        label = variations[current_var]["label"]
                    except KeyError:
                        label = None
                    if has_variations:
                        weight_name = variations[current_var]["sherpa_tag"]
                    else:
                        # When the user did not ask for any variation
                        # explicitly, we let Rivet do its thing (which might be
                        # to plot all variations).
                        weight_name = None
                    label_tag = "" if current_var == "" else "~" + current_var
                    plotinfo = get_plotinfo(comparison, len(files))
                    files.append(histogram_output_file(
                        "analysis_output", run, run_settings,
                        weight_name=weight_name,
                        tag=var_sherpa_tag,
                        label_tag=label_tag,
                        label=label,
                        dressed=dressed,
                        plotinfo=plotinfo))
                # var could instead be a combination label ...
                for combination in run_settings["combinations"]:
                    if not var == "any" and not var == combination:
                        continue
                    settings = get_combination_settings(combination)
                    if "label" in settings:
                        label_tag = "~" + settings["label"]
                    elif ("any.any" in names
                            or "any.nominal" in names
                            or run + ".any" in names
                            or run + ".nominal" in names):
                        label_tag = "~" + combination
                    else:
                        label_tag = ""
                    var_sherpa_tag = "." + combination
                    plotinfo = get_plotinfo(comparison, len(files))
                    files.append(histogram_output_file(
                        "combined_analysis_output", run, run_settings,
                        tag=var_sherpa_tag,
                        label_tag=label_tag,
                        dressed=dressed,
                        plotinfo=plotinfo))
        else:
            if name in config["global_combinations"].keys() or name == "any":
                settings = get_global_combination_settings(name)
                plotinfo = get_plotinfo(comparison, len(files))
                files.append(histogram_output_file(
                    "combined_analysis_output", name, settings,
                    dressed=dressed, plotinfo=plotinfo))
            if name in config["external_analysis_output"].keys() or name == "any":
                settings = get_external_analysis_output_settings(name)
                arg = "external_analysis_output/" + settings["file"]
                if dressed:
                    try:
                        label = settings["label"]
                    except KeyError:
                        label = name
                    arg += ":Title={}".format(label)
                    arg += ":Name={}".format(name)
                    plotinfo = get_plotinfo(comparison, len(files))
                    arg += dressing_for_plotinfo(plotinfo)
                files.append(arg)
    if config["verbose"]:
        print("Comparison input args:")
        for f in files:
            print("   ", f)
    return files


def comparison_input_names(wildcards):
    comparison = get_comparison_settings(wildcards.comparison)
    input_names = comparison["input"]
    names = []
    for name in input_names:
        if "." in name:
            run, var = name.split(".")
            if run == "any":
                runs = config["runs"].keys()
            else:
                runs = [run]
            for run in runs:
                run_settings = get_run_settings(run)
                variations = dict(get_variations(run))
                has_variations = len(variations) > 0
                variations[""] = {"sherpa_tag": "Weight"}
                for current_var in sorted(variations.keys()):
                    if (not var == "any"
                            and not current_var == var
                            and not (var == "nominal" and current_var == "")):
                        continue
                    name = "nominal"
                    if current_var == "":
                        var_sherpa_tag = ""
                    else:
                        name = current_var
                        var_sherpa_tag = "." + current_var
                    if has_variations:
                        weight_name = variations[current_var]["sherpa_tag"]
                    else:
                        # When the user did not ask for any variation
                        # explicitly, we let Rivet do its thing (which might be
                        # to plot all variations).
                        weight_name = None
                    names.append(histogram_name_with_weight_name(run, var_sherpa_tag, weight_name=weight_name))

                # var could instead be a combination label ...
                for combination in run_settings["combinations"]:
                    if not var == "any" and not var == combination:
                        continue
                    settings = get_combination_settings(combination)
                    if "label" in settings:
                        label_tag = "~" + settings["label"]
                    elif ("any.any" in input_names
                            or "any.nominal" in input_names
                            or run + ".any" in input_names
                            or run + ".nominal" in input_names):
                        label_tag = "~" + combination
                    else:
                        label_tag = ""
                    var_sherpa_tag = "." + combination
                    names.append(histogram_name(run, label_tag))
        else:
            if name in config["global_combinations"].keys() or name == "any":
                names.append(histogram_name(name))
            if name in config["external_analysis_output"].keys() or name == "any":
                names.append(histogram_name(name))
    if config["verbose"]:
        print("Comparison input names:")
        for n in names:
            print("   ", n)
    return names


def histogram_output_file(dir_name, name, settings,
    weight_name="", tag="", label_tag="", label=None, dressed=False, plotinfo=None):

    # First build the file name itself.
    f = dir_name + "/"
    if "integration" in settings:
        f += "{}.".format(settings["integration"])
    else:
        f += "global."
    f += name
    if not get_rivet_settings()["should_use_multiweights"]:
        # No multiweight usage, i.e. we need different histogram files for each variation.
        f += tag
    f += "." + get_rivet_settings()["yoda_ext"]

    # Now add potential Rivet-like dressing.
    if dressed:
        f += ':'
        if label is None:
            if "label" in settings:
                label = "{}{}".format(settings["label"], label_tag)
            else:
                label = "{}{}".format(name, label_tag)
        f += "Title={}".format(label)
        f += ":Name={}".format(histogram_name(name, label_tag))
        f += dressing_for_plotinfo(plotinfo)
        if get_rivet_settings()["should_use_multiweights"] and weight_name is not None:
            f += ":DefaultWeight={}".format(weight_name)
            f += ":Variations=none"

    return f


def histogram_name(name, label_tag=""):
    return "{}{}".format(name, label_tag[1:] if len(label_tag) > 0 else "")


def histogram_name_with_weight_name(name, label_tag="", weight_name=None):
    name = histogram_name(name, label_tag=label_tag)
    if get_rivet_settings()["should_use_multiweights"] and weight_name is not None:
            name += "[{}]".format(weight_name)
    return name


def rivetmkhtml_plot_args(wildcards):
    args_str = "PLOT"

    # add custom comparison-specific arguments
    settings = get_comparison_settings(wildcards.comparison)
    args = settings["rivetmkhtml_plot_args"]
    for key, value in args.items():
        args_str += ':{}={}'.format(key, value)

    # rivet-mkhtml will not like to see *only* PLOT on the command line,
    # instead return empty args
    if args_str == "PLOT":
        return ""

    if config["verbose"]:
        print("Comparison rivetmkhml args string:")
        print("   ", args_str)

    return args_str

# vim: ft=python
