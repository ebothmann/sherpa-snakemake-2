def insert_variation_component(output_file, variation):
    components = output_file.split('.')
    index = 1
    if components[-1] == "gz":
        index = 2
    components[-index:-index] = [variation]
    return '.'.join(components)

# vim: ft=python
