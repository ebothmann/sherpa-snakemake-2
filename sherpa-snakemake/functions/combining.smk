def combination_method(wildcards):
    if "run" in wildcards.keys():
        return get_combination_settings(wildcards.combination)["method"]
    else:
        return get_global_combination_settings(wildcards.combination)["method"]

def combination_input(wildcards):
    run_settings = get_run_settings(wildcards.run)
    names = get_combination_settings(wildcards.combination)["input"]
    if not isinstance(names, list):
        names = ["nominal"] + [val["sherpa_tag"] for key, val in get_variations(wildcards.run)]
    files = []
    for name in names:
        if name == "nominal":
            var_sherpa_tag = ""
        else:
            var_sherpa_tag = "." + name
        files.append(histogram_output_file(
            "analysis_output", wildcards.run, run_settings, tag=var_sherpa_tag))
    return files

def global_combination_input_files(wildcards, omit_variations=True):
    names = get_global_combination_settings(wildcards.combination)["input"]
    files = []
    for name in names:
        if "." in name:
            run, var = name.split(".")
            if var == "nominal":
                var_sherpa_tag = ""
            else:
                var_sherpa_tag = "." + var
            run_settings = get_run_settings(run)
            if var in run_settings["combinations"]:
                files.append(histogram_output_file(
                    "combined_analysis_output", run, run_settings, tag=var_sherpa_tag))
            else:
                if omit_variations and not var == "nominal":
                    nominal_file = histogram_output_file("analysis_output", run, run_settings) 
                    if not nominal_file in files:
                        files.append(nominal_file)
                else:
                    files.append(histogram_output_file(
                        "analysis_output", run, run_settings, tag=var_sherpa_tag))
        else:
            if name in config["global_combinations"].keys() or name == "any":
                settings = get_global_combination_settings(name)
                files.append(histogram_output_file(
                    "combined_analysis_output", name, settings))
            if name in config["external_analysis_output"].keys() or name == "any":
                settings = get_external_analysis_output_settings(name)
                files.append("external_analysis_output/" + settings["file"])
    return files

def get_combination_settings(name):
    try:
        settings = config["combinations"][name]
    except KeyError:
        settings = {}
    settings.setdefault("input", "any")
    return settings

def get_global_combination_settings(name):
    try:
        settings = config["global_combinations"][name]
    except KeyError:
        settings = {}
    return settings

# vim: ft=python
