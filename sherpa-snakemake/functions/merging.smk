import os.path as path

def existing_intermediate_analysis_output(wildcards):
    files = intermediate_analysis_output(wildcards)
    files = [f for f in files if path.exists(f)]
    return files

def existing_temporary_analysis_output(wildcards):
    files = temporary_analysis_output(wildcards)
    files = [f for f in files if path.exists(f)]
    return files

def intermediate_analysis_output(wildcards):
    run_settings = get_run_settings(wildcards.run)
    return expand("intermediate_analysis_output/{integ}.{run}.{i}." + get_rivet_settings()["yoda_ext"],
            integ=wildcards.integ,
            run=wildcards.run,
            i=range(int(run_settings["run_number_offset"]/run_settings["premerge_multiplicity"]),
                    int((run_settings["run_number_offset"] + run_settings["number_of_runs"])/run_settings["premerge_multiplicity"])))

def temporary_analysis_output(wildcards):
    run_settings = get_run_settings(wildcards.run)
    range_length = run_settings["premerge_multiplicity"]
    return expand("temporary_analysis_output/{integ}.{run}.{j}." + get_rivet_settings()["yoda_ext"],
            integ=wildcards.integ,
            run=wildcards.run,
            j=range(int(wildcards.i) * range_length, (int(wildcards.i) + 1) * range_length))

def merger_inputs(wildcards, input, output):
    inputs = [input]
    if get_rivet_settings()["should_use_multiweights"]:
        # If multi-weight capabilities are to be used, there are no histogram
        # file variants for each variation. Just return the unchanged filename
        # itself.
        return inputs
    if "i" in wildcards.keys():
        variations = [val["sherpa_tag"] for key, val in get_variations(wildcards.run).items()]
    else:
        variations = get_variations(wildcards.run).keys()
    for variation in variations:
        var_input = []
        for f in input:
            var_input.append(insert_variation_component(f, variation))
        inputs.append(var_input)
    return inputs

def merger_outputs(wildcards):
    if "i" in wildcards.keys():
        output = "intermediate_analysis_output/{}.{}.{}.{}".format(
                wildcards.integ, wildcards.run, wildcards.i, get_rivet_settings()["yoda_ext"])
    else:
        output = "analysis_output/{}.{}.{}".format(wildcards.integ, wildcards.run, get_rivet_settings()["yoda_ext"])
    outputs = [output]
    for variation in get_variations(wildcards.run).keys():
        outputs.append(insert_variation_component(str(output), variation))
    return outputs

# vim: ft=python
