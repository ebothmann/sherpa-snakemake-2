#!/usr/bin/env bash
# properties = {properties}

set -e

source /home/bothmann/.bash_profile

export PATH="/home/bothmann/miniconda3/bin:$PATH"
source activate snakemake

if [[ -r "$PBS_NODEFILE" ]] ; then
  export MPI_OPT="--hostfile $PBS_NODEFILE --np $(cat $PBS_NODEFILE|wc -l)"
  export OMP_NUM_THREADS=1
fi

{exec_job}
echo $?
