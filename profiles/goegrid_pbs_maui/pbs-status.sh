#!/usr/bin/env bash

set -e

jobid="$1"
short_jobid="${jobid%%.*}"

file=*.e$short_jobid

# NOTE: do not quote $file such that globbing works
if [ ! -r $file ] ; then
	echo running
	exit
fi

if grep -q "^Exiting because a job execution failed" $file ; then
	echo failed
fi

echo success
